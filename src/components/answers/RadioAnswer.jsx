import { Box, Grid, Typography, RadioGroup, Radio } from "@mui/material";

export default function RadioAnswer(section, surveyData, setSurveyData) {
  return (
    <div>
      <Typography
        variant={"h5"}
        sx={{
          fontWeight: 600,
          mx: {
            xs: 4,
            md: 8,
          },
          my: 4,
        }}
        align={"left"}
      >
        {section.field !== "schoolScore" ? (
          section.sectionHeader
        ) : (
          <p>
            Based on the {" "}
            {
              <a
                style={{ color: "black", textDecorationThickness: "3px" }}
                href="https://cascaid.co.uk/ofsted-inspection-framework/ratings/"
              >
                Ofsted grading system
              </a>
            }
            , what is your minimum desired school quality?
          </p>
        )}
      </Typography>
      <RadioGroup name="use-radio-group" defaultValue="first">
        {section.choices.map((choice) => (
          <RadioAnswerTemplate
            choice={choice}
            surveyData={surveyData}
            setSurveyData={setSurveyData}
            field={section.field}
          />
        ))}
      </RadioGroup>
    </div>
  );
}

const RadioAnswerTemplate = ({ choice, setSurveyData, surveyData, field }) => {
  const onRadioChange = () => {
    setSurveyData({
      ...surveyData,
      [field]: choice.code,
    });
  };
  return (
    <Box
      sx={{
        border: 1,
        borderRadius: 2,
        mx: {
          xs: 4,
          md: 8,
        },
        p: 2,
        mt: 1,
        mb: 1,
      }}
    >
      <Grid
        container
        alignItems="center"
        justifyContent="space-between"
        direction="row"
        spacing={2.5}
      >
        <Grid item>
          <Typography variant="h6" sx={{ ml: { xs: 1, md: 4 } }}>
            {choice.answer}
          </Typography>
        </Grid>
        <Grid item>
          <Radio
            value={choice.answer}
            name="radio-buttons"
            inputProps={{ "aria-label": "A" }}
            sx={{
              "& .MuiSvgIcon-root": {
                fontSize: {
                  xs: 21,
                  md: 32,
                },
              },
            }}
            onChange={onRadioChange}
          />
        </Grid>
      </Grid>
    </Box>
  );
};
