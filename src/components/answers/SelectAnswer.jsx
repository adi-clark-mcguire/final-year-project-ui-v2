import React from "react";
import _ from "lodash";

import { Box, Typography } from "@mui/material";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import ListSubheader from "@mui/material/ListSubheader";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";

import { ETHNICITY } from "../../common/constants";

export default function SelectAnswer(section, surveyData, setSurveyData) {
  const onSelectChange = (event) => {
    setSurveyData({ ...surveyData, [section.field]: event.target.value });
  };
  return (
    <Box
      sx={{
        mx: {
          xs: 4,
          md: 8,
        },
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600, mb: 1 }} align={"left"}>
        What's your ethnicity?
      </Typography>
      <FormControl fullWidth>
        <Select
          defaultValue=""
          id="select-ethnicity-label"
          label="Ethnicity"
          variant="outlined"
          value={surveyData.ethnicity}
          onChange={onSelectChange}
        >
          {_.map(ETHNICITY, (ethnicity) => {
            return [
              <ListSubheader>{ethnicity.group}</ListSubheader>,
              _.map(ethnicity.ethnicities, (option) => {
                return (
                  <MenuItem key={option.code} value={option.code}>
                    {option.label}
                  </MenuItem>
                );
              }),
            ];
          })}
        </Select>
        <InputLabel id="select-ethnicity-label">Ethnicity</InputLabel>
      </FormControl>
    </Box>
  );
}

export function EmploymentSelectAnswer(section, surveyData, setSurveyData) {
  const onSelectChange = (event) => {
    setSurveyData({ ...surveyData, [section.field]: event.target.value });
  };
  return (
    <Box
      sx={{
        mx: {
          xs: 4,
          md: 8,
        },
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600, mb: 1 }} align={"left"}>
        What's your employment status?
      </Typography>
      <FormControl fullWidth>
        <Select
          defaultValue=""
          id="select-employment-status-label"
          label="employmentStatus"
          variant="outlined"
          value={surveyData.employmentStatus}
          onChange={onSelectChange}
        >
          {_.map(section.choices, (answer) => {
            return (
              <MenuItem key={answer.code} value={answer.answer}>
                {answer.answer}
              </MenuItem>
            );
          })}
        </Select>
        <InputLabel id="select-employment-status-label">
          Employment status
        </InputLabel>
      </FormControl>
    </Box>
  );
}
