import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import Typography from "@mui/material/Typography";

import { FLEXIBILITY } from "../../common/constants";

export function FlexibilitySliderAnswer(section, surveyData, setSurveyData) {
  const handleChange = (event, newValue) => {
    setSurveyData({ ...surveyData, [section.field]: newValue });
  };

  return (
    <Box
      sx={{
        mx: 6.5,
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600 }} align={"left"}>
        {section.sectionHeader}
      </Typography>
      <Box component="Slider" sx={{ display: { xs: "block", md: "none" } }}>
        <Slider
          size="medium"
          defaultValue={2}
          aria-label="Small"
          value={surveyData[section.field]}
          valueLabelDisplay="on"
          min={1}
          max={4}
          marks
          valueLabelFormat={(value) => markers[value - 1].label}
          sx={{ mt: 5 }}
          onChange={handleChange}
        />
      </Box>
      <Box component="Slider" sx={{ display: { xs: "none", md: "block" } }}>
        <Slider
          size="medium"
          defaultValue={2}
          aria-label="Small"
          value={surveyData[section.field]}
          valueLabelDisplay="off"
          min={1}
          max={4}
          marks={markers}
          sx={{ mt: 2 }}
          onChange={handleChange}
        />
      </Box>
    </Box>
  );
}

export function ScaleSliderAnswer(section, surveyData, setSurveyData) {
  const handleChange = (event, newValue) => {
    setSurveyData({ ...surveyData, [section.field]: newValue });
  };
  const numberWithCommas = (value) => {
    return section.field === "housingBudget"
      ? `£${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
      : value === 1
      ? `${value} mile`
      : `${value} miles`;
  };

  return (
    <Box
      sx={{
        mx: 6.5,
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600 }} align={"left"}>
        {section.sectionHeader}
      </Typography>
      <Slider
        size="medium"
        defaultValue={400000}
        aria-label="Small"
        step={section.choices.step}
        value={surveyData[section.field]}
        valueLabelFormat={numberWithCommas}
        valueLabelDisplay="on"
        min={section.choices.minValue}
        max={section.choices.maxValue}
        sx={{ mt: 8 }}
        onChange={handleChange}
      />
    </Box>
  );
}

const markers = [
  {
    value: FLEXIBILITY.NOT_FLEXIBLE,
    label: "No flexibility",
  },
  {
    value: FLEXIBILITY.SOMEWHAT_FLEXIBLE,
    label: "Some flexibility",
  },
  {
    value: FLEXIBILITY.VERY_FLEXIBLE,
    label: "Very flexible",
  },
  {
    value: FLEXIBILITY.DOES_NOT_MATTER,
    label: "Doesn't matter",
  },
];
