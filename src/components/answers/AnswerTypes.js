import { ANSWER_TYPES } from "../../common/constants";
import DateAnswer from "./DateAnswer";
import { FlexibilitySliderAnswer, ScaleSliderAnswer } from "./SliderAnswer";
import RadioAnswer from "./RadioAnswer";
import TextAnswer, { LocationTextAnswer } from "./TextAnswer";
import SelectAnswer, { EmploymentSelectAnswer } from "./SelectAnswer";
import CheckboxAnswer from "./CheckboxAnswer";

export const getAnswers = (section, surveyData, setSurveyData) => {
  switch (section.responseType) {
    case ANSWER_TYPES.RADIO:
      return RadioAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.SLIDER:
      return section.field === "housingBudget" ||
        section.field === "maxDistance"
        ? ScaleSliderAnswer(section, surveyData, setSurveyData)
        : FlexibilitySliderAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.TEXT:
      return TextAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.DATE:
      return DateAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.SELECT:
      return SelectAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.CHECKBOX:
      return CheckboxAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.EMPLOYMENT:
      return EmploymentSelectAnswer(section, surveyData, setSurveyData);

    case ANSWER_TYPES.LOCATION:
      return LocationTextAnswer(section, surveyData, setSurveyData);

    default:
      return;
  }
};
