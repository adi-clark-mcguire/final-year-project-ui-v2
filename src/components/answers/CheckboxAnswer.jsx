import * as React from "react";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import Typography from "@mui/material/Typography";

export default function CheckboxAnswer(section, surveyData, setSurveyData) {
  const handleChange = (event) => {
    setSurveyData({
      ...surveyData,
      isParent: event.target.checked,
    });
  };
  return (
    <Box
      sx={{
        mx: {
          xs: 4,
          md: 8,
        },
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600 }} align={"left"}>
        {section.sectionHeader}
      </Typography>
      <Checkbox
        sx={{ "& .MuiSvgIcon-root": { fontSize: 40 } }}
        onChange={handleChange}
      />
    </Box>
  );
}
