import React from "react";
import _ from "lodash";

import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";

import { MONTHS } from "../../common/constants";

export default function DateAnswer(section, surveyData, setSurveyData) {
  const handleChangeYear = (event) => {
    setSurveyData({
      ...surveyData,
      birthDateYear: event.target.value,
    });
  };
  const handleChangeMonth = (event) => {
    setSurveyData({
      ...surveyData,
      birthDateMonth: convertMonth(event.target.value),
    });
  };
  const handleChangeDay = (event) => {
    setSurveyData({
      ...surveyData,
      birthDateDay: event.target.value,
    });
  };

  const convertMonth = (month) => {
    const formattedMonth = MONTHS.indexOf(month) + 1;
    if (formattedMonth < 10) {
      return `0${formattedMonth}`;
    }
    return formattedMonth;
  };

  return (
    <Box
      sx={{
        mx: {
          xs: 4,
          md: 8,
        },
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600 }} align={"left"}>
        Date of Birth
        {/* {section.sectionHeader} */}
      </Typography>
      <FormControl sx={{ m: 1, maxWidth: 120 }}>
        <TextField
          labelId="select-day-label"
          value={surveyData.birthDateDay}
          label="Day"
          onChange={handleChangeDay}
          inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
          helperText="E.g. 3 or 12"
        />
      </FormControl>
      <FormControl sx={{ m: 1, minWidth: 120 }}>
        <Select
          labelId="select-month-label"
          value={MONTHS[surveyData.birthDateMonth]}
          label="Month"
          onChange={handleChangeMonth}
          helperText="MM"
        >
          {_.map(MONTHS, (month) => {
            return <MenuItem value={month}>{month}</MenuItem>;
          })}
        </Select>
        <InputLabel id="select-month-label">Month</InputLabel>
      </FormControl>
      <FormControl sx={{ m: 1, minWidth: 120 }}>
        <TextField
          labelId="select-year-label"
          value={surveyData.birthDateYear}
          label="Year"
          onChange={handleChangeYear}
          inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
          helperText="E.g. 1965"
        />
      </FormControl>
    </Box>
  );
}
