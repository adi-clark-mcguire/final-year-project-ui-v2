import { Box, Typography } from "@mui/material";
import TextField from "@mui/material/TextField";
import { FormControl } from "@mui/material";

export default function TextAnswer(section, surveyData, setSurveyData) {
  const onChange = (event) => {
    setSurveyData({
      ...surveyData,
      [section.field]: event.target.value,
    });
  };
  return (
    <Box
      sx={{
        ml: 8,
        mr: 8,
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600, mb: 4 }} align={"left"}>
        {section.sectionHeader}
      </Typography>

      <FormControl fullWidth>
        <TextField
          variant="outlined"
          label={"Location"}
          onChange={onChange}
          value={surveyData[section.field]}
        />
      </FormControl>
    </Box>
  );
}

export function LocationTextAnswer(section, surveyData, setSurveyData) {
  const onChange = (event) => {
    setSurveyData({
      ...surveyData,
      [section.field]: event.target.value,
    });
  };
  return (
    <Box
      sx={{
        mx: 6.5,
        mt: 4,
      }}
    >
      <Typography variant={"h5"} sx={{ fontWeight: 600, mb: 4 }} align={"left"}>
        {section.sectionHeader}
      </Typography>

      <FormControl fullWidth>
        <TextField
          variant="outlined"
          label={"Location"}
          onChange={onChange}
          value={surveyData[section.field]}
          helperText="E.g SE10 or W1D 2HS"
        />
      </FormControl>
    </Box>
  );
}
