import React from "react";
import axios from "axios";
import _ from "lodash";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";

import { Title } from "../Title";
import {
  calculateRecommendedBoroughs,
  mapBorough,
} from "../../utils/recommendationUtils";
import { getEnv, URLS } from "../../common/urls";

export default function ContinueSearch(boroughs, setRecommendedBorough) {
  if (_.isEmpty(boroughs)) {
    return;
  }
  const recommendedBoroughs = calculateRecommendedBoroughs(boroughs);
  const fetchBorough = (index) => {
    const { boroughId, transportationScore } = recommendedBoroughs[index];

    axios.get(`${getEnv()}${URLS.borough}${boroughId}/`).then((response) => {
      setRecommendedBorough(
        mapBorough(response.data, parseFloat(transportationScore))
      );
    });
  };
  return (
    <React.Fragment>
      <Title>Other Close Matches</Title>
      <Box sx={{ alignItems: "center", width: "100%" }}>
        <ButtonGroup
          variant={"text"}
          sx={{ display: { xs: "none", md: "block" }, width: "100%" }}
        >
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(0)}
          >
            {recommendedBoroughs[0].name}
          </Button>
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(1)}
          >
            {recommendedBoroughs[1].name}
          </Button>
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(2)}
          >
            {recommendedBoroughs[2].name}
          </Button>
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(3)}
          >
            {recommendedBoroughs[3].name}
          </Button>
        </ButtonGroup>
        <ButtonGroup
          variant={"text"}
          sx={{ display: { xs: "block", md: "none" } }}
          size="large"
        >
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(0)}
          >
            {recommendedBoroughs[0].name}
          </Button>
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(1)}
          >
            {recommendedBoroughs[1].name}
          </Button>
        </ButtonGroup>
        <ButtonGroup
          variant={"text"}
          sx={{ display: { xs: "block", md: "none" } }}
          size="large"
        >
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(2)}
          >
            {recommendedBoroughs[2].name}
          </Button>
          <Button
            className={"results-page-recommended-borough-button"}
            onClick={() => fetchBorough(3)}
          >
            {recommendedBoroughs[3].name}
          </Button>
        </ButtonGroup>
      </Box>
    </React.Fragment>
  );
}
