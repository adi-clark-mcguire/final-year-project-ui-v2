import React from "react";
import { ResponsiveRadar } from "@nivo/radar";

function RadarGraph(data, surveyScores) {
  return (
    <React.Fragment>
      <ResponsiveRadar
        data={formatData(data, surveyScores)}
        keys={["You", `${data.name}`]}
        indexBy="criterion"
        valueFormat=">-.2f"
        maxValue={8}
        margin={{ top: 40, right: 60, bottom: 40, left: 80 }}
        borderColor={{ from: "color" }}
        gridLabelOffset={18}
        dotSize={10}
        dotColor={{ theme: "background" }}
        dotBorderWidth={2}
        colors={{ scheme: "accent" }}
        blendMode="multiply"
        motionConfig="wobbly"
        legends={[
          {
            anchor: "top-left",
            direction: "column",
            translateX: -50,
            translateY: -40,
            itemWidth: 80,
            itemHeight: 20,
            itemTextColor: "#999",
            symbolSize: 12,
            symbolShape: "circle",
            effects: [
              {
                on: "hover",
                style: {
                  itemTextColor: "#000",
                },
              },
            ],
          },
        ]}
      />
    </React.Fragment>
  );
}

const formatData = (borough, user) => {
  return [
    {
      criterion: "Schooling",
      You: user.school_score,
      [borough.name]: borough.schoolScore,
    },
    {
      criterion: "Crime",
      You: user.crime_score,
      [borough.name]: borough.crimeScore,
    },
    {
      criterion: "Distance",
      You: 3.5,
      [borough.name]: borough.transportationScore,
    },
    {
      criterion: "Housing",
      You: user.housing_score,
      [borough.name]: borough.houseScore,
    },
  ];
};

export default RadarGraph;
