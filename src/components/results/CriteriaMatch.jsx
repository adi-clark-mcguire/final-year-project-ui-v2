import React, { useState } from "react";
import axios from "axios";
import _ from "lodash";

import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Skeleton from "@mui/material/Skeleton";
import Typography from "@mui/material/Typography";
import HomeIcon from "@mui/icons-material/Home";
import SchoolIcon from "@mui/icons-material/School";
import TrainIcon from "@mui/icons-material/Train";
import SecurityIcon from "@mui/icons-material/Security";

import { ResponsiveRadialBar } from "@nivo/radial-bar";

import { Title, Subtitle } from "../Title";
import { getTflTransportTimeURL } from "../../common/urls";

const primaryColour = "#23408e";

export default function CriteriaMatch(
  loading,
  userScore,
  recommendedBorough,
  surveyData,
  userCoords
) {
  const [travelTime, setTravelTime] = useState(0);

  getTravelTime(userCoords, recommendedBorough, setTravelTime);

  return (
    <React.Fragment>
      <Grid container item direction="row" spacing={2}>
        {getGradeSection(
          gradesSections[0],
          loading,
          userScore,
          recommendedBorough,
          surveyData,
          userCoords,
          travelTime
        )}
        {getGradeSection(
          gradesSections[3],
          loading,
          userScore,
          recommendedBorough,
          surveyData,
          userCoords,
          travelTime
        )}
        {getGradeSection(
          gradesSections[1],
          loading,
          userScore,
          recommendedBorough,
          surveyData,
          userCoords,
          travelTime
        )}
        {getGradeSection(
          gradesSections[2],
          loading,
          userScore,
          recommendedBorough,
          surveyData,
          userCoords,
          travelTime
        )}
      </Grid>
    </React.Fragment>
  );
}

const getGradeSection = (
  section,
  loading,
  userScore,
  recommendedBorough,
  surveyData,
  userCoords,
  travelTime,
  setTravelTime
) => {
  const matchPercentage = Math.abs(
    getMatchPercentage(
      userScore[section.fieldName1],
      recommendedBorough[section.fieldName2]
    )
  );
  section.data[0].data[0].y = matchPercentage;
  return (
    <Grid
      item
      md={3}
      xs={12}
      justifyContent="center"
      alignItems="center"
    >
      <Box
        sx={{
          borderRadius: 3,
          backgroundColor: "#FFFDFA",
          minHeight: 300,
          px: 4,
        }}
      >
        {loading ? (
          <Skeleton sx={{ ml: 2 }} variant="text" animation="wave">
            <Typography variant="h3">Loading</Typography>
          </Skeleton>
        ) : (
          <React.Fragment>
            <Title pl={2}>
              {section.title}
              {section.icon}
            </Title>
            <Subtitle px={6}>
              {section.title === "Transportation" ? (
                <div className="results-page-pie-chart">
                  <p>
                    The centre of {recommendedBorough.name} is{" "}
                    <span className="results-page-grade">
                      {recommendedBorough.transportationScore.toFixed(1)} miles{" "}
                    </span>
                    from {_.toUpper(surveyData.location)}.
                  </p>
                  <p style={{ paddingTop: 5, paddingBottom: 5 }}>
                    That's {travelTime} minutes via public transport.
                  </p>
                </div>
              ) : (
                <p>
                  <span className="results-page-grade">
                    {matchPercentage}%{" "}
                  </span>
                  match.
                </p>
              )}
            </Subtitle>
          </React.Fragment>
        )}
        <div>
          {section.title === "Transportation" ? (
            <div></div>
          ) : (
            <div className="results-page-pie-chart">
              <ResponsiveRadialBar
                data={section.data}
                margin={{ top: 4, right: 8, bottom: 8, left: 8 }}
                maxValue={100}
                endAngle={360}
                colors={{ scheme: "accent" }}
                innerRadius={0.55}
                enableRadialGrid={false}
                enableCircularGrid={false}
                radialAxisStart={null}
                circularAxisOuter={null}
              />
            </div>
          )}
        </div>
      </Box>
    </Grid>
  );
};

const getTravelTime = (user, borough, setTravelTime) => {
  axios
    .get(
      getTflTransportTimeURL(
        { lat: borough.latitude, lng: borough.longitude },
        { lat: user.latitude, lng: user.longitude }
      )
    )
    .then((response) => {
      setTravelTime(response.data.journeys[0].duration);
    });
};

const gradesSections = [
  {
    title: "Schooling",
    fieldName1: "school_score",
    fieldName2: "schoolScore",
    icon: <SchoolIcon sx={{ fontSize: 32, color: primaryColour }} />,
    data: [
      {
        id: "school",
        label: "School Match",
        data: [
          {
            x: "school",
          },
        ],
      },
    ],
  },
  {
    title: "Housing",
    fieldName1: "housing_score",
    fieldName2: "houseScore",
    icon: <HomeIcon sx={{ fontSize: 32, color: primaryColour }} />,
    data: [
      {
        id: "housing",
        label: "Housing Match",
        data: [
          {
            x: "housing",
          },
        ],
      },
    ],
  },
  {
    title: "Transportation",
    fieldName1: "distance_score",
    fieldName2: "transportationScore",
    icon: <TrainIcon sx={{ fontSize: 32, color: primaryColour }} />,
    data: [
      {
        id: "transport",
        label: "Transportation Match",
        data: [
          {
            x: "transport",
          },
        ],
      },
    ],
  },
  {
    title: "Crime",
    fieldName1: "crime_score",
    fieldName2: "crimeScore",
    icon: <SecurityIcon sx={{ fontSize: 32, color: primaryColour }} />,
    data: [
      {
        id: "crime",
        label: "Crime Match",
        data: [
          {
            x: "crime",
          },
        ],
      },
    ],
  },
];

function getMatchPercentage(userScore, boroughScore) {
  const score = (boroughScore / userScore) * 100;
  
  const adjScore = score % 100;
  return score < 100 ? score.toFixed(2) : (100 - adjScore).toFixed(2);
}
