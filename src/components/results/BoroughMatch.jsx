import React from "react";
import _ from "lodash";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import { getInMyAreaURL } from "../../common/urls";

export default function BoroughMatch(borough) {
  const onClick = () => {
    window.open(getInMyAreaURL(borough.name), '_blank');
  }
  return (
    <React.Fragment>
      <Box>
        <Typography
          component={"h2"}
          variant={"h4"}
          className={"results-page-borough-title"}
        >
          {_.toUpper(borough.name)}.
        </Typography>
        <Typography
          component={"h5"}
          variant={"h6"}
          className={"results-page-borough-subtitle"}
        >
          By our calculations, this borough and you seem like a great match!
        </Typography>
        <Button
          sx={{ bgcolor: "#FFFFFF", mr: 0, mb: 0, color: "black" }}
          variant="contained"
          onClick={onClick}
        >
          Discover More
        </Button>
      </Box>
    </React.Fragment>
  );
}
