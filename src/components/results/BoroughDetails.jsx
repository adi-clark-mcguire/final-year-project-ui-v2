import React from "react";

import { Markup } from "interweave";
import Box from "@mui/material/Box";
import { Title, Subtitle } from "../Title";

export default function BoroughDetails(borough) {
  return (
    <React.Fragment>
      <Box>
        <Title variant="h4">About the area</Title>
        <Markup className={"subtitle-text"} content={borough.description} />
        <Subtitle>From london.gov.uk</Subtitle>
      </Box>
    </React.Fragment>
  );
}
