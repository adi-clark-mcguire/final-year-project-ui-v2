import React, { Component } from "react";
import GoogleMapReact from 'google-map-react';


import { API_KEY } from "../../api-key";

export class BoroughMap extends Component {
  render() {
    return (
      <div
        style={{
          height: 240,
          position: "relative",
          width: "100%",
        }}
      >
        <GoogleMapReact
          defaultZoom={12}
          bootstrapURLKeys={{ key: API_KEY }}
          center={{
            lat: parseFloat(this.props.latitude),
            lng: parseFloat(this.props.longitude),
          }}
          defaultCenter={{
            lat: 51.49472,
            lng: -0.135278,
          }}
        />
      </div>
    );
  }
}

export default BoroughMap;