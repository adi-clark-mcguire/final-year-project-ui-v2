import * as React from "react";
import PropTypes from "prop-types";
import Typography from "@mui/material/Typography";

export function Title(props) {
  return (
    <Typography
      component="h2"
      variant="h6"
      color="#212121"
      gutterBottom
      className={"title-text"}
      sx={{ pl: props.pl, pt: 2 }}
    >
      {props.children}
    </Typography>
  );
}

Title.propTypes = {
  children: PropTypes.node,
};

export function Subtitle(props) {
  return (
    <Typography
      color="#484848"
      gutterBottom
      className={"subtitle-text"}
      sx={{ pl: 2 }}
    >
      {props.children}
    </Typography>
  );
}

Subtitle.propTypes = {
  children: PropTypes.node,
};
