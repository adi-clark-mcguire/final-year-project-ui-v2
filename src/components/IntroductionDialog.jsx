import * as React from "react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Typography from "@mui/material/Typography";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function IntroductionDialog() {
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          sx={{ m: 0, p: 2 }}
        >
          Introduction
        </DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Thank you for helping me test my final year project app!
          </Typography>
          <Typography sx={{fontWeight: 900, mt: 4}}gutterBottom>What does it do?</Typography>
          <Typography gutterBottom>
            Property searching can be difficult. Trovi aims to lend a helping
            hand by doing the thinking for you. Just answer several quick
            questions and it will match you to where suits you in London!
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            Close
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}
