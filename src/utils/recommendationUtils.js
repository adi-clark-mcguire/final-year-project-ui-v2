import _ from "lodash";

export function calculateClosestBorough(boroughs) {
  const userScores = boroughs.pop();

  const scoredBoroughs = _.map(boroughs, (borough) => {
    return {
      boroughId: borough.borough_id,
      distanceScore: getDistance(userScores, borough),
      transportationScore: borough.distance_score,
    };
  });

  boroughs.push(userScores);

  const sortedBoroughs = _.sortBy(scoredBoroughs, (borough) => {
    return borough.distanceScore;
  });

  return {
    boroughId: sortedBoroughs[0].boroughId,
    transportationScore: sortedBoroughs[0].transportationScore,
  };
}

export function calculateRecommendedBoroughs(boroughs) {
  const userScores = boroughs.pop();

  const scoredBoroughs = _.map(boroughs, (borough) => {
    return {
      boroughId: borough.borough_id,
      name: borough.borough_name,
      distanceScore: getDistance(userScores, borough),
      transportationScore: borough.distance_score,
    };
  });

  boroughs.push(userScores);

  const sortedBoroughs = _.sortBy(scoredBoroughs, (borough) => {
    return borough.distanceScore;
  });

  return [
    {
      boroughId: sortedBoroughs[0].boroughId,
      transportationScore: sortedBoroughs[0].transportationScore,
      name: sortedBoroughs[0].name,
    },
    {
      boroughId: sortedBoroughs[1].boroughId,
      transportationScore: sortedBoroughs[1].transportationScore,
      name: sortedBoroughs[1].name,
    },
    {
      boroughId: sortedBoroughs[2].boroughId,
      transportationScore: sortedBoroughs[2].transportationScore,
      name: sortedBoroughs[2].name,
    },
    {
      boroughId: sortedBoroughs[3].boroughId,
      transportationScore: sortedBoroughs[3].transportationScore,
      name: sortedBoroughs[3].name,
    },
  ];
}

function getDistance(user, borough) {
  return Math.sqrt(
    Math.pow(user.mds_x - borough.mds_x, 2) +
      Math.pow(user.mds_y - borough.mds_y, 2)
  );
}

export const mapBorough = (boroughData, distanceScore) => {
  return {
    id: boroughData.borough_id,
    name: boroughData.borough_name,
    description: boroughData.description,
    latitude: parseFloat(boroughData.latitude),
    longitude: parseFloat(boroughData.longitude),
    schoolScore: parseFloat(boroughData.schoolleague.school_score),
    houseScore: parseFloat(boroughData.housepriceaverage.house_price_score),
    crimeScore: parseFloat(boroughData.crimereport.crime_score),
    transportationScore: parseFloat(distanceScore),
    housePriceAverage: parseInt(boroughData.housepriceaverage.mean_house_price),
  };
};
