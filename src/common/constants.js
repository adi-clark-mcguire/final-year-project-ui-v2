export const ANSWER_TYPES = {
  SLIDER: "SLIDER",
  RADIO: "RADIO",
  DATE: "DATE",
  TEXT: "TEXT",
  SELECT: "SELECT",
  CHECKBOX: "CHECKBOX",
  EMPLOYMENT: "EMPLOYMENT",
};

export const MONTHS = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export const GENDERS = {
  FEMALE: "Female",
  MALE: "Male",
  NON_BINARY: "Non-binary",
  OTHER: "Other",
  NA: "N/A",
};

export const AVERAGES = {
  EXCELLENT: 1,
  ABOVE: 2,
  AVERAGE: 3,
  POOR: 4,
};

export const OFSTED_SCORES = {
  OUTSTANDING: 1,
  GOOD: 2,
  SATISFACTORY: 3,
  INADEQUATE: 4,
};

export const IS_BUYING = {
  BUYING: true,
  RENTING: false,
};

export const EMPLOYMENT_STATUS = {
  FULL_TIME: "A",
  PART_TIME: "B",
  RETIRED: "C",
  UNEMPLOYED: "D",
  SABATICAL: "E",
  STUDENT: "F",
  MATERNITY: "G",
  OTHER: "H",
};

export const ETHNICITY = {
  asian: {
    group: "Asian or Asian British",
    ethnicities: [
      { label: "Indian", code: "A" },
      { label: "Pakistani", code: "B" },
      { label: "Bangladeshi", code: "C" },
      { label: "Chinese", code: "D" },
      { label: "Any other Asian background", code: "E" },
    ],
  },
  black: {
    group: "Black, Black British, Caribbean or African",
    ethnicities: [
      { label: "Caribbean", code: "F" },
      { label: "African", code: "G" },
      {
        label: "Any other Black, Black British, or Caribbean background",
        code: "H",
      },
    ],
  },
  mixed: {
    group: "Mixed or multiple ethnic groups",
    ethnicities: [
      { label: "White and Black Caribbean", code: "I" },
      { label: "White and Black African", code: "J" },
      { label: "White and Asian", code: "K" },
      { label: "Any other Mixed or multiple ethnic background", code: "L" },
    ],
  },
  white: {
    group: "White",
    ethnicities: [
      {
        label: "English, Welsh, Scottish, Northern Irish or British",
        code: "M",
      },
      { label: "Irish", code: "N" },
      { label: "Gypsy or Irish Traveller", code: "O" },
      { label: "Roma", code: "P" },
      { label: "Any other White background", code: "Q" },
    ],
  },
  other: {
    group: "Other ethnic group",
    ethnicities: [
      { label: "Arab", code: "R" },
      { label: "Any other ethnic group", code: "S" },
    ],
  },
};

export const FLEXIBILITY = {
  NOT_FLEXIBLE: 4,
  SOMEWHAT_FLEXIBLE: 3,
  VERY_FLEXIBLE: 2,
  DOES_NOT_MATTER: 1,
};

export const IS_PARENT = {
  PARENT: true,
  NOT_PARENT: false,
};
