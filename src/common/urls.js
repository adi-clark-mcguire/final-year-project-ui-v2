import _ from "lodash";

export const getEnv = () => {
  return ENVS.PROD;
};

const ENVS = {
  DEV: "trovi/",
  PROD: "http://trovi.herokuapp.com/trovi/",
};
export const URLS = {
  getBoroughPlots: "survey/",
  postSurvey: "survey/",
  borough: "borough/",
};

export const getInMyAreaURL = (borough) => {
  return `https://www.london.gov.uk/in-my-area/${_.kebabCase(borough)}`;
};

export const getZooplaURL = (borough, maxPrice) => {
  return `https://www.zoopla.co.uk/for-sale/property/london/${_.kebabCase(
    borough
  )}/?page_size=25&price_max=${maxPrice}&view_type=list&q=${_.replace(
    borough,
    " ",
    "%20"
  )}&radius=0&results_sort=newest_listings&search_source=facets`;
};

export const getTflTransportTimeURL = (start, end) => {
  return `https://api.tfl.gov.uk/journey/journeyresults/${start.lat},${start.lng}/to/${end.lat},${end.lng}`;
};
