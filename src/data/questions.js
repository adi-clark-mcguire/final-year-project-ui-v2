import {
  ANSWER_TYPES,
  AVERAGES,
  EMPLOYMENT_STATUS,
  GENDERS,
  IS_BUYING,
  IS_PARENT,
  OFSTED_SCORES,
} from "../common/constants";

const schoolQuestion = {
  question:
    "Education is essential if you've got children. Let us know if that's important to you",
  answer: [
    {
      responseType: ANSWER_TYPES.RADIO,
      sectionHeader: "What is your minimum desired school quality level?",
      answers: ["Above average", "Average", "Below average"],
      field: "schoolScore",
      choices: [
        {
          answer: "Outstanding",
          code: OFSTED_SCORES.OUTSTANDING,
        },
        {
          answer: "Good",
          code: OFSTED_SCORES.GOOD,
        },
        {
          answer: "Satisfactory",
          code: OFSTED_SCORES.SATISFACTORY,
        },
        {
          answer: "Inadequate",
          code: OFSTED_SCORES.INADEQUATE,
        },
      ],
    },
    {
      responseType: ANSWER_TYPES.SLIDER,
      sectionHeader: "How much of a priority is this?",
      field: "schoolPriority",
    },
  ],
};

const crimeQuestion = {
  question: "When moving somewhere new, it's critical you feel safe",
  answer: [
    {
      responseType: ANSWER_TYPES.RADIO,
      sectionHeader: "What is the minimum level of crime you would tolerate?",
      field: "crimeScore",
      choices: [
        {
          answer: "Extremely low",
          code: AVERAGES.EXCELLENT,
        },
        {
          answer: "Low",
          code: AVERAGES.ABOVE,
        },
        {
          answer: "High",
          code: AVERAGES.AVERAGE,
        },
        {
          answer: "Extremely high",
          code: AVERAGES.POOR,
        },
      ],
    },
    {
      responseType: ANSWER_TYPES.SLIDER,
      sectionHeader: "How flexible are you on this?",
      field: "crimePriority",
      choices: [
        {
          answer: "Full-time",
          code: EMPLOYMENT_STATUS.FULL_TIME,
        },
        {
          answer: "Part-time",
          code: EMPLOYMENT_STATUS.PART_TIME,
        },
      ],
    },
  ],
};

const housingQuestion = {
  question: "What is your budget on buying a home?",
  answer: [
    {
      responseType: ANSWER_TYPES.SLIDER,
      sectionHeader: "Housing budget:",
      field: "housingBudget",
      choices: {
        minValue: 100000,
        maxValue: 1500000,
        step: 50000,
      },
    },
    {
      responseType: ANSWER_TYPES.SLIDER,
      sectionHeader: "How flexible is your budget?",
      answers: ["Full-time", "Part-time", "Retired", "Unemployed", "Sabatical"],
      field: "housingPriority",
    },
  ],
};

const distanceQuestion = {
  question: "It's good to be nearby to friends and family or work",
  answer: [
    {
      responseType: ANSWER_TYPES.LOCATION,
      sectionHeader:
        "Tell us a postcode in London where you'd like to be near. For instance a place of work or a relative",
      field: "location",
    },
    {
      responseType: ANSWER_TYPES.SLIDER,
      sectionHeader: "What is the furthest you would be?",
      field: "maxDistance",
      choices: {
        minValue: 1,
        maxValue: 15,
        step: 1,
      },
    },
    {
      responseType: ANSWER_TYPES.SLIDER,
      sectionHeader: "How much of a priority to you is this?",
      field: "distancePriority",
    },
  ],
};

const demographicQuestion1 = {
  question: "Finally, let us know a few extra details about yourself",
  answer: [
    {
      responseType: ANSWER_TYPES.RADIO,
      sectionHeader: "What is your gender?",
      answers: ["Female", "Male", "Non-binary", "Other", "Prefer not to say"],
      field: "gender",
      choices: [
        {
          answer: "Female",
          code: GENDERS.FEMALE,
        },
        {
          answer: "Male",
          code: GENDERS.MALE,
        },
        {
          answer: "Non-binary",
          code: GENDERS.NON_BINARY,
        },
        {
          answer: "Other",
          code: GENDERS.OTHER,
        },
        {
          answer: "Prefer not to say",
          code: GENDERS.NA,
        },
      ],
    },
    {
      responseType: ANSWER_TYPES.RADIO,
      sectionHeader: "What sort of property are you looking for?",
      answers: ["To buy", "To rent"],
      field: "isRenting",
      choices: [
        {
          answer: "Buying",
          code: IS_BUYING.BUYING,
        },
        {
          answer: "Renting",
          code: IS_BUYING.RENTING,
        },
      ],
    },
    {
      responseType: ANSWER_TYPES.EMPLOYMENT,
      sectionHeader: "What's your employment status?",
      answers: [
        "Full-time",
        "Part-time",
        "Retired",
        "Unemployed",
        "Sabatical",
        "Student",
        "Maternity/Paternity",
        "Other",
      ],
      field: "employmentStatus",
      choices: [
        {
          answer: "Full-time",
          code: EMPLOYMENT_STATUS.FULL_TIME,
        },
        {
          answer: "Part-time",
          code: EMPLOYMENT_STATUS.PART_TIME,
        },
        {
          answer: "Retired",
          code: EMPLOYMENT_STATUS.RETIRED,
        },
        {
          answer: "Unemployed",
          code: EMPLOYMENT_STATUS.UNEMPLOYED,
        },
        {
          answer: "Student",
          code: EMPLOYMENT_STATUS.STUDENT,
        },
        {
          answer: "Maternity/Paternity",
          code: EMPLOYMENT_STATUS.MATERNITY,
        },
        {
          answer: "Other",
          code: EMPLOYMENT_STATUS.OTHER,
        },
      ],
    },
  ],
};

const demographicQuestion2 = {
  question: "Finally, let us know a few extra details about yourself",
  answer: [
    {
      responseType: ANSWER_TYPES.SELECT,
      sectionHeader: "What's your ethnicity",
      field: "ethnicity",
    },
    {
      responseType: ANSWER_TYPES.DATE,
      sectionHeader: "What's your date of birth?",
      field: "birthDate",
    },
    {
      responseType: ANSWER_TYPES.CHECKBOX,
      sectionHeader: "Please tick if you are a parent",
      answers: ["Yes", "No"],
      field: "isParent",
      choices: [
        {
          answer: "Parent",
          code: IS_PARENT.PARENT,
        },
        {
          answer: "Not a parent",
          code: IS_PARENT.NOT_PARENT,
        },
      ],
    },
  ],
};

export const questionAnswers = [
  schoolQuestion,
  crimeQuestion,
  housingQuestion,
  distanceQuestion,
  demographicQuestion1,
  demographicQuestion2,
];
