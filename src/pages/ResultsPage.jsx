import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import axios from "axios";
import Geocode from "react-geocode";
import _ from "lodash";

import { API_KEY } from "../api-key";
import { getEnv, getZooplaURL, URLS } from "../common/urls";

import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Skeleton from "@mui/material/Skeleton";
import Typography from "@mui/material/Typography";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

import NavBar from "../components/NavBar";
import { Title } from "../components/Title";
import CriteriaMatch from "../components/results/CriteriaMatch";
import ContinueSearch from "../components/results/ContinueSearch";
import BoroughDetails from "../components/results/BoroughDetails";
import BoroughMatch from "../components/results/BoroughMatch";
import BoroughMap from "../components/results/BoroughMap";

import { initialSurveyAnswers } from "../pages/QuestionPage";
import RadarGraph from "../components/results/RadarGraph";
import {
  calculateClosestBorough,
  mapBorough,
} from "../utils/recommendationUtils";

const boroughPlaceholder = {
  id: undefined,
  name: "Borough",
  description: "Description",
  latitude: 0,
  longitude: 0,
  location: 0,
  schoolScore: 0,
  houseScore: 0,
  crimeScore: 0,
  transportationScore: 0,
  housePriceAverage: 0,
};

const ResultsPage = () => {
  const location = useLocation();
  const surveyData = location?.state?.surveyData || initialSurveyAnswers;
  const [boroughData, setBoroughData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [userScores, setUserScores] = useState({
    schoolScore: 0,
    crimeScore: 0,
    housingScore: 0,
    transportationScore: 0,
  });
  const [recommendedBorough, setRecommendedBorough] =
    useState(boroughPlaceholder);
  const [userCoords, setUserCoords] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("Error.");

  // when page mounts, fetch borough details from recommended boroughs
  useEffect(() => {
    if (!_.isNil(surveyData.location)) {
      fetchData(surveyData, setBoroughData, loading, setLoading);
    }
  }, []);

  const fetchBorough = ({ boroughId, transportationScore }) => {
    axios.get(`${getEnv()}${URLS.borough}${boroughId}/`).then((response) => {
      setRecommendedBorough(
        mapBorough(response.data, parseFloat(transportationScore))
      );
    });
  };

  const fetchData = async () => {
    Geocode.fromAddress(surveyData.location).then(
      (response) => {
        const { lat, lng } = response.results[0].geometry.location;
        const surveyDataRequest = mapSurveyAnswer(surveyData);

        setUserCoords({ latitude: lat, longitude: lng });

        console.log("STATUS");
        console.log(response.status);
        if (response.status === "OK") {
          surveyDataRequest.latitude = lat;
          surveyDataRequest.longitude = lng;

          axios
            .post(`${getEnv()}${URLS.postSurvey}`, surveyDataRequest)
            .then(
              (response) => {
                setBoroughData(response.data);
                fetchBorough(calculateClosestBorough(response.data));
                setUserScores(response.data[32]);
              },
              (error) => {
                setIsOpen(true);
                setLoading(false);
                setErrorMessage(
                  "Please check that you answered all the questions."
                );
              }
            )
            .finally(() => {
              setLoading(false);
            });
        } else {
          setIsOpen(true);
          setLoading(false);
          setErrorMessage(
            "Please check the location that you wanted to be close to was valid."
          );
        }
      },
      (error) => {
        setIsOpen(true);
        setLoading(false);
        console.log("Couldn't fetched address: ");
        console.log(error);
      }
    );
  };

  const handleClose = () => {
    window.history.back();
    setIsOpen(!isOpen);
  };

  return (
    <div className="results-page-page">
      <NavBar boroughData={boroughData} />
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="primary" />
        <Box
          sx={{
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            position: "absolute",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Typography>Calculating your results...</Typography>
        </Box>
      </Backdrop>
      <Container
        className="results-page-container"
        maxWidth="lg"
        sx={{ pb: 4 }}
      >
        <Box className="results-page-box" sx={{ display: "flex" }}>
          <Dialog
            open={isOpen}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Oops</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Something went wrong calculating your answers. {errorMessage}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Redo Survey</Button>
            </DialogActions>
          </Dialog>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6}>
              <Paper
                sx={{
                  p: 2,
                  display: "flex",
                  minHeight: 240,
                  background:
                    "linear-gradient(006deg, rgba(086,205,102,1) 0%, rgba(140,158,255,1) 100%)",
                }}
              >
                {loading ? (
                  <Skeleton
                    variant="rectangle"
                    sx={{
                      bgcolor: "grey.200",
                      opacity: "20%",
                      width: "100%",
                      height: "140px",
                      borderRadius: 1,
                    }}
                  />
                ) : (
                  BoroughMatch(recommendedBorough)
                )}
              </Paper>
            </Grid>
            <Grid item xs={12} md={6}>
              <Paper
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  p: 2,
                  minHeight: 240,
                }}
              >
                {loading ? (
                  <React.Fragment>
                    <Skeleton variant={"text"} height={100} />
                    <Skeleton variant={"text"} height={100} />
                  </React.Fragment>
                ) : (
                  BoroughDetails(recommendedBorough)
                )}
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper
                sx={{
                  p: 2,
                  display: "flex",
                  flexDirection: "column",
                  backgroundColor: "#FFCC99",
                }}
              >
                {CriteriaMatch(
                  loading,
                  userScores,
                  recommendedBorough,
                  surveyData,
                  userCoords
                )}
              </Paper>
            </Grid>
            <Grid item xs={12} md={6}>
              <Paper
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  p: 2,
                }}
              >
                <Title>Area</Title>
                <BoroughMap
                  latitude={recommendedBorough.latitude}
                  longitude={recommendedBorough.longitude}
                />
              </Paper>
            </Grid>
            <Grid item xs={12} md={6}>
              {loading ? (
                <Skeleton
                  variant="rectangle"
                  height={300}
                  sx={{ borderRadius: 1 }}
                />
              ) : (
                <Paper
                  sx={{
                    p: 2,
                    display: "flex",
                    flexDirection: "column",
                    height: 300,
                    backgroundColor: "",
                  }}
                >
                  {RadarGraph(recommendedBorough, userScores)}
                </Paper>
              )}
            </Grid>
            <Grid item xs={12} md={7}>
              <Paper
                sx={{
                  px: 2,
                  py: 3,
                }}
              >
                {ContinueSearch(boroughData, setRecommendedBorough)}
              </Paper>
            </Grid>
            <Grid item container xs={12} md={5} spacing={1} direction={"row"}>
              <Grid item xs={12}>
                <Paper
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    alignSelf: "center",
                    justifyItems: "center",
                  }}
                >
                  <Link
                    to={`/graph`}
                    className={"results-page-graph-view-btn"}
                    style={styles.link}
                    state={{ boroughData: boroughData }}
                  >
                    GRAPH VIEW
                  </Link>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    alignSelf: "center",
                    justifyItems: "center",
                  }}
                >
                  <Button
                    onClick={() =>
                      window.open(
                        getZooplaURL(
                          recommendedBorough.name,
                          surveyData.housingBudget
                        ),
                        "_blank"
                      )
                    }
                    variant={"outlined"}
                    style={styles.zooplaButton}
                  >
                    {`Browse ${recommendedBorough.name} properties on Zoopla`}
                  </Button>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </div>
  );
};

const styles = {
  link: {
    textDecoration: "none",
    color: "white",
    fontWeight: 600,
    border: "1px",
    borderRadius: "5px",
    width: "100%",
    paddingBottom: "15px",
    paddingTop: "15px",
    textAlign: "center",
  },
  zooplaButton: {
    textDecoration: "none",
    color: "#8046F1",
    fontWeight: 600,
    borderColor: "#8046F1",
    borderRadius: "5px",
    width: "100%",
    paddingBottom: "15px",
    paddingTop: "15px",
    textAlign: "center",
  },
};

export default ResultsPage;

Geocode.setApiKey(API_KEY);

const mapSurveyAnswer = (surveyData) => {
  return {
    survey_start_time: surveyData.surveyStartTime,
    survey_end_time: new Date(),
    gender: surveyData.gender,
    ethnicity: surveyData.ethnicity,
    date_of_birth: `${surveyData.birthDateYear}-${surveyData.birthDateMonth}-${surveyData.birthDateDay}`,
    is_parent: surveyData.isParent,
    is_renting: surveyData.isRenting,
    latitude: null,
    longitude: null,
    school_weighting: surveyData.schoolPriority,
    school_desired_quality: surveyData.schoolScore,
    crime_weighting: surveyData.crimePriority,
    crime_desired_quality: surveyData.crimeScore,
    distance_weighting: surveyData.distancePriority,
    max_distance: surveyData.maxDistance,
    average_house_price_weighting: surveyData.housingPriority,
    house_price_max: surveyData.housingBudget,
    employment_status: surveyData.employmentStatus,
  };
};
