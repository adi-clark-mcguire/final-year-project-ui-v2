import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

import axios from "axios";
import _ from "lodash";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import { ResponsiveScatterPlot } from "@nivo/scatterplot";

import { getEnv, URLS } from "../common/urls";
import NavBar from "../components/NavBar";
import { Title } from "../components/Title";
import { ButtonGroup } from "@mui/material";


export const GraphPage = () => {
  const location = useLocation();
  const boroughData = location?.state?.boroughData;

  const [graphData, setGraphData] = useState([]);

  useEffect(() => {
    getGraphData(boroughData, setGraphData);
  }, []);

  useEffect(() => {
    getGraphData(boroughData, setGraphData);
  }, [boroughData]);

  const fetchGraphData = async () => {
    const data = await axios.get(`${getEnv()}${URLS.getBoroughPlots}`);
    const plots = [];
    _.forEach(data.data, (plot) => {
      plots.push({
        id: plot.borough_name,
        data: [{ x: plot.mds_x, y: plot.mds_y }],
      });
    });
    setGraphData(plots);
  };

  return (
    <div className="graph-page-page">
      <NavBar />
      <Container className="graph-page-container">
        <Title>Plotted Graph of London Boroughs</Title>
        <ButtonGroup variant="outlined">
          <Button sx={{ pl: 2, mt: 2, ml: 2 }} onClick={() => fetchGraphData()}>
            Fetch Borough Data
          </Button>
          <Button
            sx={{ pl: 2, mt: 2, ml: 2 }}
            onClick={() => window.history.back()}
          >
            Back to Results
          </Button>
        </ButtonGroup>

        <Box className="graph-page-graph">
          <ResponsiveScatterPlot
            data={graphData}
            margin={{ top: 60, bottom: 40, left: 32, right: 32 }}
            xScale={{ type: "linear", min: "auto", max: "auto" }}
            yScale={{ type: "linear", min: "auto", max: "auto" }}
            nodeSize={(node) => {
              return node.serieId === "You!" ? 24 : 12;
            }}
            colors={{ scheme: "set2" }}
          />
        </Box>
      </Container>
    </div>
  );
};

const getGraphData = (boroughData, setGraphData) => {
  const plots = [];
  if (boroughData !== null || boroughData !== undefined) {
    _.forEach(boroughData, (plot) => {
      plots.push({
        id: plot.borough_name,
        data: [{ x: plot.mds_x, y: plot.mds_y }],
      });
    });
  }
  setGraphData(plots);
};
