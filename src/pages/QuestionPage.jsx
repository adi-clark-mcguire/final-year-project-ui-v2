import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import LinearProgress from "@mui/material/LinearProgress";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CssBaseline from "@mui/material/CssBaseline";

import { getAnswers } from "../components/answers/AnswerTypes";
import { questionAnswers } from "../data/questions";
import IntroductionDialog from "../components/IntroductionDialog";

export const initialSurveyAnswers = {
  surveyStartTime: null,
  surveyEndTime: null,
  schoolScore: 0,
  schoolPriority: 1,
  crimeScore: 0,
  crimePriority: 1,
  housingBudget: 750000,
  housingPriority: 1,
  distancePriority: 1,
  maxDistance: 0,
  location: "",
  isParent: false,
  gender: null,
  employmentStatus: null,
  ethnicity: null,
  birthDateDay: null,
  birthDateMonth: null,
  birthDateYear: null,
};

export const QuestionPage = (setBoroughData) => {
  const [surveyData, setSurveyData] = useState(initialSurveyAnswers);
  const [questionNumber, setQuestionNumber] = useState(0);

  useEffect(() => {
    surveyData.surveyStartTime = new Date();
  }, []);

  const nextButton = (index) => {
    window.scrollTo(0, 0);
    if (index < questionAnswers.length && index >= 0) {
      setQuestionNumber(index);
    }
  };
  const calculateProgress = () => {
    return 100 * (questionNumber / questionAnswers.length);
  };

  return (
    <Grid className={"container"} spacing={0} sx={{ height: "100vh" }}>
      <CssBaseline />
      <Grid
        container
        sx={{
          background:
            "linear-gradient(180deg, rgba(92,107,192,1) 0%, rgba(0,150,136,1) 50%)",
        }}
        item
        md={6}
        xs={12}
        className={"question-column"}
      >
        <Typography
          className={"question"}
          variant="h4"
          sx={{
            color: "white",
            px: "1",
            fontWeight: "bold",
          }}
          align="left"
        >
          {questionAnswers[questionNumber].question}
        </Typography>
      </Grid>
      <Grid
        md={6}
        xs={12}
        alignItems={"center"}
        justifyContent={"center"}
        direction={"column-reverse"}
        className={"answer-column"}
        item
        container
      >
        <Grid
          sx={{
            alignItems: "center",
          }}
        >
          <div>
            {questionAnswers[questionNumber].answer.map((section) =>
              getAnswers(section, surveyData, setSurveyData)
            )}
          </div>
        </Grid>
        <Grid style={style.footer} xs={12} md={6}>
          <LinearProgress
            sx={{ mb: 2, height: "2.5px" }}
            variant="determinate"
            value={calculateProgress()}
          />
          <IntroductionDialog />
          <Grid item container justifyContent={"space-around"}>
            <Grid item>
              <Button
                sx={{ color: "black" }}
                onClick={() => nextButton(questionNumber - 1)}
              >
                Back
              </Button>
            </Grid>
            <Grid item>
              {questionNumber + 1 !== questionAnswers.length ? (
                <Button
                  sx={{ bgcolor: "black", mr: 0 }}
                  variant="contained"
                  style={style.finishButton}
                  onClick={() => nextButton(questionNumber + 1)}
                >
                  NEXT
                </Button>
              ) : (
                <Box>
                  <Link
                    style={style.finishButton}
                    to="/results"
                    state={{ surveyData: surveyData }}
                  >
                    FINISH
                  </Link>
                </Box>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

const style = {
  header: {
    position: "fixed",
    width: "50%",
    top: "0px",
    right: "0px",
    backgroundColor: "white",
  },
  footer: {
    position: "fixed",
    height: "60px",
    width: "100%",
    bottom: "0px",
    right: "0px",
    backgroundColor: "white",
    zIndex: 1,
    marginTop: "20px",
  },
  radio: {
  },
  finishButton: {
    padding: 9,
    backgroundColor: "black",
    fontSize: 14,
    borderRadius: "5px",
    textDecoration: "none",
    color: "white",
  },
};
