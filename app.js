import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import "./App.css";
import { QuestionPage } from "./pages/QuestionPage";
import ResultsPage from "./pages/ResultsPage";
import { GraphPage } from "./pages/GraphPage";
import "./styles/common.css";
import "./styles/QuestionPage.css";
import "./styles/ResultsPage.css";
import "./styles/GraphPage.css";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="" element={<QuestionPage />} />
        <Route path="/questions" element={<QuestionPage />} />
        <Route path="/results" element={<ResultsPage />} />
        <Route path="/graph" element={<GraphPage />} />
      </Routes>
    </Router>
  );
}

export default App;
