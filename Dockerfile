FROM node:16.4.1
WORKDIR /final-year-project-ui-v2
COPY package.json ./
RUN npm install
COPY . .
CMD ["npm", "start"]